package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;

import static javafx.application.Platform.exit;

public class Main extends Application {

    private static String name;
    private static String server;
    private static Integer port;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Chat");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
        Controller controller = loader.getController();
        controller.start(name, server, port);
        primaryStage.setOnCloseRequest( (event -> exit()));

    }


    public static void main(String[] args) {
        if (args.length == 3) {
            try {
                port = Integer.valueOf(args[2]);
            } catch (NumberFormatException exception) {
                System.out.println("Please, enter valid port number");
                return;
            }
            server = args[1];
            name = args[0];
        } else {
            System.out.println("Required args: nickname port");
            return;
        }
        launch(args);

    }
}