package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class Controller {

    private Connection connection;

    @FXML
    VBox msgBox;

    @FXML
    Button btnSend;

    @FXML
    TextField tfMsgText;

    public void send() {
        if (!tfMsgText.textProperty().getValue().equals("")) {
            connection.send(tfMsgText.textProperty().getValue());
            tfMsgText.textProperty().setValue("");
        }
    }

    public void start(String name, String server, Integer port) {
        connection = new Connection(name, server, port);
        connection.setOnIncomeMessageListener(str -> {
            Label label = new Label(str);
            msgBox.getChildren().add(label);
        });
    }

}
