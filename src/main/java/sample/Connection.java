package sample;

import javafx.application.Platform;
import message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketException;


public class Connection implements Runnable {

    private Socket socket;
    private String name;
    private String server;
    private Thread thread;
    private int port;
    private IncomeMessageListener listener;
    ObjectInputStream inp;
    ObjectOutputStream out;

    public Connection(String username, String server, int port) {
        this.port = port;
        this.name = username;
        this.server = server;
        thread = new Thread(this);
        thread.setDaemon(true);
        connect();
    }

    @Override
    public void run() {
        try {
            Message response = new Message();
            response.setSender(name);
            response.setType(Message.Type.REGISTRATION);
            send(response);
            response.setType(Message.Type.MESSAGE);
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    return;
                }
                try {
                    response = (Message) inp.readObject();
                    final String s = response.getText();
                    if (listener != null) {
                        Platform.runLater(() -> listener.onIncomeMessageAction(s));
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SocketException e) {
                    System.out.println("Connection lost");
                    Platform.exit();
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOnIncomeMessageListener(IncomeMessageListener listener) {
        this.listener = listener;
    }


    public boolean connect() {
        if (thread.isAlive())
            return false;
        try {
            this.socket = new Socket(server, port);
            out = new ObjectOutputStream(socket.getOutputStream());
            inp = new ObjectInputStream(socket.getInputStream());
            thread.start();
        } catch (IOException e) {
            System.out.println("Cannot connect");
            return false;
        }
        return true;
    }

    private void send(Message msg) {
        try {
            out.flush();
            out.writeObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String str) {
        try {
            out.flush();
            Message msg = new Message();
            msg.setType(Message.Type.MESSAGE);
            msg.setSender(name);
            msg.setText(str);
            out.writeObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public interface IncomeMessageListener {
        void onIncomeMessageAction(String str);
    }

}
